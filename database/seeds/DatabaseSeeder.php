<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        if(DB::table('hak_akses')->get()->count() == 0){

            DB::table('hak_akses')->insert([

                [
              'hak_akses'  => 'admin',
               'created_at'      => \Carbon\Carbon::now('Asia/Jakarta'),
                'updated_at'      => \Carbon\Carbon::now('Asia/Jakarta')
            
            ],
            [
              'hak_akses'  => 'staf',
               'created_at'      => \Carbon\Carbon::now('Asia/Jakarta'),
                'updated_at'      => \Carbon\Carbon::now('Asia/Jakarta')
            ],

            ]);

        } else { echo "\e[31mTable is not empty, therefore NOT "; }


        


        
    }
}
