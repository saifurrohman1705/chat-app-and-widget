<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama', 45);
            $table->string('email',30)->unique();
            $table->string('password',30);
            $table->text('alamat');
            $table->integer('id_hak_akses')->unsigned();
            $table->timestamps();
            $table->softDeletes();

            
        });

        Schema::table('users',function(Blueprint $table){
            
            $table->foreign('id_hak_akses')->references('id')->on('hak_akses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}

