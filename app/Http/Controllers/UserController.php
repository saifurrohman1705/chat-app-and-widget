<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Hash;

class UserController extends Controller
{
    public function viewUser()
    {
        
        $datas['user'] = User::all();
        
        return view('pages.user.privilege',$datas);

    }

    public function viewEdit($id)
    {
        $data['data'] = User::findOrFail($id);

        return view('pages.edit',$data);
    }

    public function editUser(Request $request)
    {
        $this->validate($request,[
            'nama' =>'string',
            'email' => 'string|email|max:255',
            'password' => 'required|string|confirmed',
            'alamat' =>'string',
         ]);

        $user = User::findOrFail($request->id);

        if(Hash::check($request->password,$user->password)){
            if($user->fill($request->except('password','password_confirmation','id'))->save()){
                return redirect()->route('user.privilege');
            }else{
                return redirect()->route('user.edit.view',['id'=>$user->id])->with('status','Proses Edit Gagal');
            }

        }else{
            return redirect()->route('user.edit.view',['id'=>$user->id])->with('status','Password yang anda masukan salah');
        }


    }
    
    public function deleteUser($id)
    {
        $user = User::findOrFail($id);

        if($user->delete()){
            return redirect()->route('user.privilege')->with('status','User Berhasil dihapus');
        }else{
            return redirect()->route('user.privilege')->with('status','User Gagal dihapus');
        }

    }

    public function changePrivilege($id)
    {
        $user = User::findOrFail($id);

        if($user->id_hak_akses == 1){
            $user->id_hak_akses = 2;
        }else{
            $user->id_hak_akses = 1;
        }

        if($user->save()){
            return redirect()->route('user.privilege')->with('status','Hak Akses Berhasil dirubah');
        }else{
            return redirect()->route('user.privilege')->with('status','Hak Akses Gagal dirubah');
        }
        

        
    }
}
