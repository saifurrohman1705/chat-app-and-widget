<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;


class AuthController extends Controller
{
    public function login(Request $request){
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
    		return redirect()->route('dashboard');
    	}else{
    		return redirect('/')->with('status','Mohon Email dan Password anda');
    	}
    }

    public function signUp(Request $request)
    {

        $this->validate($request,[
            'namaLengkap' =>'required|string',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|confirmed',
            'alamat' =>'required|string',
         ]);

        try{
            $user = new User;
            $user->nama = $request->namaLengkap;
            $user->email = $request->email;
            $user->password = bcrypt($request->password);
            $user->id_hak_akses = 2;
            $user->alamat = $request->alamat;
            if($user->save() == true){
                return redirect('/')->with('status','Akun Berhasil Dibuat');
            }
        }catch(Exception $e){
            $data= ['status'=> 'gagal','msg'=>$e->getMessage()];
            return redirect('/register',$data);
        }
    }

    public function logout()
    {
        Auth::logout();

        return redirect('/');
    }
}
