<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Privilege extends Model
{
    //
    public function users()
    {
        return $this->belongsTo('App\User');
    }

    protected $table = 'hak_akses';
}
