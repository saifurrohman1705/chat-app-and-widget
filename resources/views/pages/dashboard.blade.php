@extends('Template.template')

@section('content')

    <h1>
      Dashboard
      <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Dashboard</li>
    </ol>

    <h2>Selamat Datang {{Auth::user()->nama}}</h2>
 
@endsection
