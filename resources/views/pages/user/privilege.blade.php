@extends('Template.template') @section('content')



          <section class="content-header">
            <h1>
              Hak Akses User

            </h1>

          </section>


          <div class="box-body table-responsive no-padding">





            <table id="example2" class="table table-bordered table-hover">
              <thead>
                <tr>
                  <th style="width:30%">Nama</th>
                  <th style="width:25%">Email</th>
                  <th style="width:25%">Alamat</th>
                  <th style="width:15%">Action</th>

                </tr>
              </thead>
              <tbody>

                @foreach($user as $users) @if(Auth::user()->id_hak_akses == 1 || Auth::user()->id == $users->id)
                <tr>
                  <td>{{$users->nama}}</td>
                  <td>{{$users->email}}</td>
                  <td>{{$users->alamat}}</td>
                  <td>
                    <a href="{{route('user.edit.view',['id'=>$users->id])}}" class="btn btn-xs btn-warning">Edit</a>
                    @if(Auth::user()->id_hak_akses == 1) @if($users->id_hak_akses == 1)
                    <a href="{{route('user.edit.privilege',['id'=>$users->id])}}" class="btn btn-xs btn-default">Staff </a>
                    @else
                    <a href="{{route('user.edit.privilege',['id'=>$users->id])}}" class="btn btn-xs btn-default">Admin</a>
                    @endif
                    <a href="" onclick="hapusUser({{$users->id}})" class="btn btn-xs btn-danger">Delete</a>
                    @endif
                  </td>
                </tr>
                @endif @endforeach

              </tbody>
            </table>






          </div>


@endsection @section('script') function hapusUser(id) { if (confirm("Apakah anda yakin ingin menghapus user ini?")) { document.location.href=
'{{url('user/deleteUser')}}/'+id; } else { } } @endsection