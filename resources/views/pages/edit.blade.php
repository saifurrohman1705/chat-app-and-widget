@extends('Template.template') @section('content')

      <!-- left column -->
      
         <section class="content-header">
          <h1>
            Edit Data User
          </h1>

        </section>
        <div class="box box-primary">
          <div class="box-header with-border">
            @if (session('status'))
            <div class="alert alert-info col-md-5 alert-dismissible">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              {{ session('status') }}
            </div>
            @endif @if ($errors->any())
            <div class="alert alert-info col-md-5 alert-dismissible">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              <ul style="list-style-type: none">
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div>
            @endif
          </div>


          <form role="form" action="{{route('user.edit')}}" method="POST">
            {{csrf_field()}}
            <div class="box-body">
              <div class="form-group">
                <label for="Email">Email address</label>
                <input type="email" class="form-control" id="email" value="{{$data->email}}" placeholder="Enter email" readonly>
                <input type="hidden" name="id" value="{{$data->id}}">
              </div>
              <div class="form-group">
                <label for="Email">Nama Lengkap</label>
                <input type="text" class="form-control" id="namaLengkap" name="nama" value="{{$data->nama}}" placeholder="Enter email">
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">Password</label>
                <input type="password" class="form-control" id="password" name="password" placeholder="Password">
              </div>
              <div class="form-group">
                <label for="exampleInputPassword1">Password</label>
                <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="Password">
              </div>

              <div class="form-group">
                <textarea name="alamat" class="form-control" id="alamat" cols="50" rows="3" style="resize:none" placeholder="Alamat">{{$data->alamat}}</textarea>

              </div>
            </div>


            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
          </form>
        </div>






@endsection