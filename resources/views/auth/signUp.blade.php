<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>E-Office | Register</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{ asset('assets/bootstrap/dist/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('assets/font-awesome/css/font-awesome.min.css') }}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{asset('assets/Ionicons/css/ionicons.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset ('assets/dist/css/AdminLTE.min.css')}}">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{asset('assets/plugins/iCheck/square/blue.css')}}">

 
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>


  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition register-page">
        <div class="register-box">
          <div class="register-logo">
            <a href=""><b>E-</b>Office</a>
          </div>

          @if ($errors->any())
            <div class="alert alert-danger">
              <ul style="list-style-type: none">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
                  </ul>
              </div>
          @endif
                  
          <div class="register-box-body">
            <p class="login-box-msg">Register a new membership</p>
        
            <form action="{{route('signUp')}}" method="post">
                {{ csrf_field()}}
              <div class="form-group has-feedback">
                <input type="text" class="form-control" name="namaLengkap" placeholder="Nama Lengkap">
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
              </div>
              <div class="form-group has-feedback">
                <input type="email" class="form-control" name="email" placeholder="Email">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
              </div>
              <div class="form-group has-feedback">
                <input type="password" class="form-control" name="password" placeholder="Password">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
              </div>
              <div class="form-group has-feedback">
                <input type="password" class="form-control" name="password_confirmation" placeholder="Konfirmasi Password">
                <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
              </div>
              <div class="form-group has-feedback">
                    <textarea name="alamat" class="form-control" id="alamat" cols="50" rows="3" style="resize:none" placeholder="Alamat"></textarea>
                  </div>
            <div class="row">
                <div class="col-xs-8">
                 
                </div>
                <!-- /.col -->
                <div class="col-xs-4">
                  <button type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
                </div>
                <!-- /.col -->
              </div>
            </form>
        
            <a href="/" class="text-center">Sudah punya akun?</a>
          </div>
          <!-- /.form-box -->
        </div>
        <!-- /.register-box -->
        
        <!-- jQuery 3 -->
        <script src="{{ asset('assets/jquery/dist/jquery.min.js')}}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{asset('assets/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- iCheck -->
<script src="{{asset ('assets/plugins/iCheck/icheck.min.js')}}"></script>
        <script>
          $(function () {
            $('input').iCheck({
              checkboxClass: 'icheckbox_square-blue',
              radioClass: 'iradio_square-blue',
              increaseArea: '20%' /* optional */
            });
          });


          @if (!empty($status))
              alert({{$msg}});
          @endif
        </script>
        </body>
</html>
