<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();


Route::get('/', function () {
    return view('auth.login');
})->middleware('guest');

Route::get('/register',function(){
    return view('auth.signUp'); 
})->middleware('guest');


Route::group(['prefix' => 'Auth'], function() {
    Route::post('signIn', 'AuthController@login')->name('signIn');
    Route::post('signUp', 'AuthController@signUp')->name('signUp');
    Route::get('logout','AuthController@logout')->name('logout');
});


Route::group(['prefix' => 'home','middleware'=>'auth'], function() {
    Route::get('dashboard','HomeController@dashboard')->name('dashboard');

});




Route::middleware(['auth'])->group(function(){

    Route::resource('currency','CurrencyController');

    Route::group(['prefix' => 'user'], function() {
        Route::get('userPrivilege', 'UserController@viewUser')->name('user.privilege');
        Route::get('editUser/{id}','UserController@viewEdit')->name('user.edit.view');
        Route::get('deleteUser/{id}','UserController@deleteUser')->name('user.delete');
        Route::post('edit','UserController@editUser')->name('user.edit');
        Route::get('changePrivilege/{id}','UserController@changePrivilege')->name('user.edit.privilege');



    });

});

